package farm.cocaine.speedrunplugin;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.*;
import org.bukkit.advancement.Advancement;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.graalvm.compiler.lir.StandardOp;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.bukkit.Bukkit.getServer;
import static org.bukkit.util.NumberConversions.toInt;

public class Speedrun implements Listener {
    Party party;
    Timestamp starttime;
    Player player;
    long time;
    BukkitRunnable runnable;
    Location spawn;
    boolean inNether = false;
    String exitNether = "";
    long[] split = new long[5];

    Speedrun(Player player, Party party, Timestamp timestamp) {
        this.party = party;
        this.starttime = timestamp;
        this.player = player;
        Plugin x = Bukkit.getPluginManager().getPlugin("SpeedrunPlugin");
        getServer().getPluginManager().registerEvents(this, x);

    }

    public String getTimeSplit(String splitName) {
        long t;
        switch (splitName) {
            case "nether":
                t = split[0];
                break;
            case "fortress":
                t = split[1];
                break;
            case "exit_nether":
                t = split[2];
                break;
            case "end":
                t = split[3];
                break;
            case "kill_enderdragon":
                t = split[4];
                break;
            default:
                t = -1;
                break;
        };
        if (t == 0)
            return "";
        Date d = new Date(t * 1000L);
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        String time = df.format(d);
        return time;
    }

    public void stop() {
        Plugin x = Bukkit.getPluginManager().getPlugin("SpeedrunPlugin");
        PlayerRespawnEvent.getHandlerList().unregister(x);
        PlayerAdvancementDoneEvent.getHandlerList().unregister(x);
        PlayerPortalEvent.getHandlerList().unregister(x);
        runnable.cancel();
    }

    public void stop_movement() {
        Vector<PotionEffect> effects = new Vector<PotionEffect>();
        PotionEffect jump = new PotionEffect(PotionEffectType.JUMP, 999999, 128);
        effects.add(jump);
        PotionEffect walk = new PotionEffect(PotionEffectType.SLOW, 999999, 255);
        effects.add(walk);
        PotionEffect see = new PotionEffect(PotionEffectType.BLINDNESS, 999999, 255);
        effects.add(see);
        PotionEffect mine = new PotionEffect(PotionEffectType.SLOW_DIGGING, 999999, 255);
        effects.add(mine);
        player.addPotionEffects(effects);
    }

    public void start_movement() {
        player.removePotionEffect(PotionEffectType.JUMP);
        player.removePotionEffect(PotionEffectType.SLOW);
        player.removePotionEffect(PotionEffectType.BLINDNESS);
        player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
    }

    public void start() {
        Iterator<Advancement> iterator = getServer().advancementIterator();
        while(iterator.hasNext()){
            AdvancementProgress progress = player.getAdvancementProgress(iterator.next());
            for (String criteria : progress.getAwardedCriteria())
                progress.revokeCriteria(criteria);
        }
        player.getInventory().clear();
        player.setHealth(20);
        player.setFoodLevel(25);
        player.setExp(0);
        this.stop_movement();
        Plugin x = Bukkit.getPluginManager().getPlugin("SpeedrunPlugin");
        runnable = new BukkitRunnable() {
            @Override
            public void run() {
                while(true) {
                    Timestamp now = new Timestamp(System.currentTimeMillis());
                    if(now.getTime() < starttime.getTime()) {
                        int sec = toInt(Math.ceil((starttime.getTime() - now.getTime()) / 1000.0));
                        player.sendMessage(ChatColor.YELLOW + "" +  sec);
                        if (sec <= 3) {
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1,1);
                        }
                        try {
                            TimeUnit.SECONDS.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        runnable = new BukkitRunnable() {
                            @Override
                            public void run() {
                                spawn = player.getLocation();
                                player.setBedSpawnLocation(player.getLocation());
                                start_movement();
                            }
                        };
                        runnable.runTask(x);
                        break;
                    }
                }
                runnable = new BukkitRunnable() {
                    @Override
                    public void run() {
                        Date d = new Date(time * 1000L);
                        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                        df.setTimeZone(TimeZone.getTimeZone("GMT"));
                        String t = df.format(d);
                        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(ChatColor.YELLOW + "" + t));
                        time++;
                    }
                };
                runnable.runTaskTimerAsynchronously(x, 0, 20);
            }
        };
        runnable.runTaskAsynchronously(x);
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        if(event.getPlayer() == player) {
            Location location;
            Location bed = player.getBedSpawnLocation();
            if(bed == null)
                 location = new Location(Bukkit.getWorld(player.getName()), spawn.getX(), spawn.getY(), spawn.getZ());
            else
                location = new Location(Bukkit.getWorld(player.getName()), bed.getX(), bed.getY(), bed.getZ());
            event.setRespawnLocation(location);
        }
    }

    @EventHandler
    public void onAdvancementDone(PlayerAdvancementDoneEvent e){
        Player p = e.getPlayer();
        if(p == player) {
            switch (e.getAdvancement().getKey().getKey()) {
                case "nether/find_fortress":
                    split[2] = 0;
                    split[1] = time;
                    party.send_time_to_players(time, player, "Nether Fortress");
                    break;
                case "story/enter_the_end":
                    split[3] = time;
                    party.send_time_to_players(time, player, "Enter the End");
                    break;
                case "end/kill_dragon":
                    split[4] = time;
                    party.send_time_to_players(time, player, "Free the End");
                    break;
            }
        }
    }

    @EventHandler
    public void onEntityPortalEnter(PlayerPortalEvent event) {
        PortalType type;
        if (event.getCause() == PlayerTeleportEvent.TeleportCause.END_PORTAL) {
            type = PortalType.ENDER;
        } else if (event.getCause() == PlayerTeleportEvent.TeleportCause.NETHER_PORTAL) {
            type = PortalType.NETHER;
        } else {
            return;
        }
        Player p = event.getPlayer();
        if(p == player) {
            Iterator<Advancement> iterator = getServer().advancementIterator();
            while(iterator.hasNext()){
                Advancement y = iterator.next();
                String x = y.getKey().getKey();
                if (x.contains("enter_the_nether") && type == PortalType.NETHER) {
                    AdvancementProgress progress = player.getAdvancementProgress(y);
                    if(!inNether) {
                        if (!progress.isDone()) {
                            getServer().dispatchCommand(getServer().getConsoleSender(), "advancement grant " + player.getName() + " only minecraft:story/enter_the_nether");
                            split[0] = time;
                            party.send_time_to_players(time, player, "Enter the Nether");
                        }
                        inNether = true;
                    } else {
                        split[2] = time;
                        party.send_time_to_players(time, player, "Exit the Nether");
                        inNether = false;
                    }
                    return;
                } else if (x.contains("end") && type == PortalType.ENDER) {
                    AdvancementProgress progress = player.getAdvancementProgress(y);
                    if(!progress.isDone())
                        getServer().dispatchCommand(getServer().getConsoleSender(), "advancement grant " + player.getName() + " only minecraft:story/enter_the_end");
                    else
                        party.send_time_to_players(time, player, "Free the End");
                    return;
                }
            }
        }
    }
}
