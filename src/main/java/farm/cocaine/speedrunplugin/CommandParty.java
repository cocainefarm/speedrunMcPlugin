package farm.cocaine.speedrunplugin;

import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;
import org.bukkit.advancement.Advancement;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import net.md_5.bungee.api.chat.TextComponent;

import java.awt.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.bukkit.Bukkit.getServer;

public class CommandParty implements CommandExecutor, Listener {

    //Party[] parties = new Party[0];
    Map<String, Party> partys = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length == 0) {
            return  list(sender);
        } else {
            switch (args[0]) {
                case "create":
                    if (args.length < 2)
                        break;
                    return create(sender, args[1]);
                case "list":
                    return list(sender);
                case "invite":
                    if (args.length < 2)
                        break;
                    return invite(sender, args[1]);
                case "accept":
                    return accept(sender);
                case "leave":
                    break;
                case "remove":
                    break;
                case "start":
                    return start(sender);
                case "restart":
                    return restart(sender);
                case "delete":
                    break;
            }
        }
        //sender.sendMessage(command.getName());
        return false;
    }

    public boolean create(CommandSender sender, String name) {
        if(partys.containsKey(sender.getName())) {
            sender.sendMessage(ChatColor.RED + "Your are allready in a party");
            return true;
        }
        if(!partys.isEmpty()) {
            for (Map.Entry<String, Party> entry : partys.entrySet()) {
                if (Arrays.asList(entry.getValue().getPlayers()).contains(sender)) {
                    sender.sendMessage(ChatColor.RED + "Your are allready in a party");
                }
            }
        }

        Party party = new Party(name, (Player) sender);
        partys.put(sender.getName(), party);
        sender.sendMessage(ChatColor.GREEN + "Party " + name + " created. Invite other players with /party invite <playername>");
        return true;
    }

    public boolean invite(CommandSender sender, String p) {
        if(sender.getName().equals(p)) {
            sender.sendMessage(ChatColor.RED + "You cannot invite your self");
            return true;
        }
        if(!partys.containsKey(sender.getName())) {
            sender.sendMessage(ChatColor.RED + "Only party owner can invite other players");
            return true;
        }
        Player player = getServer().getPlayer(p);
        if(!partys.isEmpty()) {
            for (Map.Entry<String, Party> entry : partys.entrySet()) {
                if (entry.getValue().isInParty(player)) {
                    sender.sendMessage(ChatColor.RED + "Player is allready in a party");
                    return true;
                } else if (entry.getValue().isInvited(player)) {
                    sender.sendMessage(ChatColor.RED + "Player is allready invited");
                    return true;
                }
            }
        }
        if(player == null) {
            sender.sendMessage(ChatColor.RED + "User not found");
            return true;
        }
        Party party = partys.get(sender.getName());
        party.invitePlayer(player);
        TextComponent msg = new TextComponent(ChatColor.YELLOW + "You got invitet by " + sender.getName() + " for a vs speedrun");
        TextComponent accept = new TextComponent(ChatColor.GREEN + "[ACCEPT]");
        accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/party accept"));
        TextComponent space = new TextComponent(ChatColor.RED + " ");
        TextComponent decline = new TextComponent(ChatColor.RED + "[DECLINE]");
        decline.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/party decline"));
        accept.addExtra(space);
        accept.addExtra(decline);
        player.spigot().sendMessage(msg);
        player.spigot().sendMessage(accept);
        return true;
    }

    public boolean accept(CommandSender sender) {
        if(!partys.isEmpty()) {
            for (Map.Entry<String, Party> entry : partys.entrySet()) {
                Party party = entry.getValue();
                if(party.isInvited((Player) sender)) {
                    Player owner = party.getOwner();
                    sender.sendMessage(ChatColor.GREEN + "You joined the party of " + owner.getName());
                    owner.sendMessage(ChatColor.GREEN + sender.getName() + " joined your party");
                    party.addPlayer((Player) sender);
                    return true;
                }
            }
        }
        return true;
    }

    public boolean list(CommandSender sender) {
        if(partys.containsKey(sender.getName())) {
            for(Player p: partys.get(sender.getName()).getPlayers())
                sender.sendMessage(ChatColor.YELLOW + p.getName());
            return true;
        }
        if(!partys.isEmpty()){
            for (Map.Entry<String, Party> entry : partys.entrySet()) {
                if (Arrays.asList(entry.getValue().getPlayers()).contains(sender)) {
                    for(Player p: partys.get(sender.getName()).getPlayers())
                        sender.sendMessage(ChatColor.YELLOW + p.getName());
                    return true;
                }
            }
        }
        sender.sendMessage(ChatColor.RED + "Your not in any party");
        return true;
    }

    public boolean start(CommandSender sender) {
        if(partys.containsKey(sender.getName())) {
            Party party = partys.get((sender.getName()));
            for(Player p: party.getPlayers()){
                p.sendMessage(ChatColor.YELLOW + "Starting....");
            }
            party.start();
            return true;
        }
        sender.sendMessage(ChatColor.RED + "Your not in a party or not the owner");
        return true;
    }

    public boolean restart(CommandSender sender) {
        if(partys.containsKey(sender.getName())) {
            sender.sendMessage(ChatColor.YELLOW + "Restarting");
            Party party = partys.get(sender.getName());
            party.restart();
        } else {
            sender.sendMessage(ChatColor.RED + "only the owner can restart");
        }
        return true;
    }
}
