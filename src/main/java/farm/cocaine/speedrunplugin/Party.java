package farm.cocaine.speedrunplugin;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

import com.onarandombox.MultiverseCore.api.MVDestination;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import com.onarandombox.MultiverseCore.api.Teleporter;
import com.onarandombox.MultiverseCore.commands.EnvironmentCommand;
import com.onarandombox.MultiverseCore.destination.CustomTeleporterDestination;
import com.onarandombox.MultiverseCore.destination.DestinationFactory;
import com.onarandombox.MultiverseCore.enums.TeleportResult;
import com.onarandombox.MultiverseCore.event.MVTeleportEvent;
import com.onarandombox.MultiverseCore.utils.WorldManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import com.onarandombox.MultiverseCore.MultiverseCore;
import org.bukkit.WorldType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.*;

public class Party {
    private String name;
    private Player owner;
    private Player[] players;
    private boolean ownerWorld = false;
    private boolean playerWorld = true;
    private Map<String, Speedrun> runs = new HashMap<>();
    Map<Player, Timestamp> invited = new HashMap<>();
    private int playerIndex = 0;
    private boolean firstLoop = true;
    private Scoreboard board;
    private Objective objective;

    public Party(String name, Player owner) {
        this.name = name;
        this.owner = owner;
        this.players = new Player[] {owner};
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                createOwnerWorld();
            }
        });
        t.run();
    }

    public String getName() {
        return name;
    }

    public Player getOwner() {
        return owner;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void invitePlayer(Player player) {
        Timestamp now = new Timestamp(System.currentTimeMillis() + 15000);
        invited.put(player, now);
    }

    public void addPlayer(Player player) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                createPlayerWorld(player);
            }
        });
        t.run();
        Player[] new_players = new Player[players.length +1];
        for(int i = 0; i < players.length; i++) {
            new_players[i] = players[i];
        }
        new_players[players.length] = player;
        players = new_players;
        if(invited.containsKey(player))
            invited.remove(player);
    }

    public void remove_player(Player player) {
        if (Arrays.asList(players).contains(player)) {
            Player[] new_palyers = new Player[players.length -1];
            for(int i = 0, j = 0; i < players.length; i++) {
                if (players[i] != player) {
                    new_palyers[j] = players[i];
                    j++;
                }
            }
            players = new_palyers;
        }
    }

    public void send_time_to_players(long time, Player player, String split) {
        Date d = new Date(time * 1000L);
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        String t = df.format(d);
        for(Player p: players) {
            p.sendMessage(ChatColor.YELLOW + player.getName() + " | " + split + " split -> " + t);
        }
    }

    public void sendMsgToPlayers(String msg) {
        for(Player player: players) {
            player.sendMessage(ChatColor.YELLOW + msg);
        }
    }

    public boolean isInParty(Player player) {
        return Arrays.asList(players).contains(player);
    }

    public boolean isInvited(Player player) {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        if(invited.containsKey(player))
            if(invited.get(player).getTime() > now.getTime())
                return true;
            else {
                player.sendMessage(ChatColor.RED + "You took to long to accept");
                invited.remove(player);
            }
        return false;
    }

    public void createOwnerWorld() {
        ownerWorld = false;
        deleteWorld(owner.getName());
        MultiverseCore core = (com.onarandombox.MultiverseCore.MultiverseCore)
                Bukkit.getPluginManager().getPlugin("Multiverse-Core");
        WorldManager wmg = (WorldManager) core.getMVWorldManager();

        sendMsgToPlayers("Creating overworld");
        String seed = String.valueOf(createWorld(wmg, owner.getName(), "", "NORMAL"));

        sendMsgToPlayers("Creating Nether");
        createWorld(wmg, owner.getName() + "_nether", seed, "NETHER");

        sendMsgToPlayers("Creating End");
        createWorld(wmg, owner.getName() + "_the_end", seed, "END");
        ownerWorld = true;
    }

    public void createPlayerWorld(Player player) {
        playerWorld = false;
        MultiverseCore core = (com.onarandombox.MultiverseCore.MultiverseCore)
                Bukkit.getPluginManager().getPlugin("Multiverse-Core");
        WorldManager wmg = (WorldManager) core.getMVWorldManager();
        deleteWorld(player.getName());
        String ownerName = owner.getName();
        String name = player.getName();

        sendMsgToPlayers("Coping overwold for " + name);
        copyWorld(wmg, ownerName, name);

        sendMsgToPlayers("Coping nether for " + name);
        copyWorld(wmg, ownerName + "_nether", name + "_nether");

        sendMsgToPlayers("Coping end for " + name);
        copyWorld(wmg, ownerName + "_the_end", name + "_the_end");
        playerWorld = true;
    }

    public void copyWorld(WorldManager wmg, String srcName, String desName) {
        wmg.cloneWorld(srcName, desName);
        wmg.loadWorld(desName);
    }

    public long createWorld(WorldManager wmg, String name, String seed, String t) {
        World.Environment environment = EnvironmentCommand.getEnvFromString(t);
        WorldType type = EnvironmentCommand.getWorldTypeFromString(t);
        wmg.addWorld(name, environment, seed, type, true, null, true);
        MultiverseWorld world = wmg.getMVWorld(name);
        return world.getSeed();
    }

    //TODO load them first
    public void deleteWorld(String name) {
        sendMsgToPlayers("Deleting old worlds");
        MultiverseCore core = (com.onarandombox.MultiverseCore.MultiverseCore)
                Bukkit.getPluginManager().getPlugin("Multiverse-Core");
        WorldManager wmg = (WorldManager) core.getMVWorldManager();
        wmg.loadWorld(name);
        wmg.loadWorld(name + "_nether");
        wmg.loadWorld(name + "_the_end");
        File worldFile = new File(core.getServer().getWorldContainer(), name);
        if (worldFile.exists()) {
            wmg.deleteWorld(name);
            wmg.deleteWorld(name + "_nether");
            wmg.deleteWorld(name + "_the_end");
        }
    }

    private void setScoreBoard(int i, String time) {
        if (i == 0)
            return;
        Score score;
        switch (i) {
            case 9:
                score = objective.getScore(ChatColor.GREEN + "Enter Nether");
                score.setScore(10);
                break;
            case 7:
                score = objective.getScore(ChatColor.GREEN + "Enter Fortress");
                score.setScore(8);
                break;
            case 5:
                score = objective.getScore(ChatColor.GREEN + "Exit Nether");
                score.setScore(6);
                break;
            case 3:
                score = objective.getScore(ChatColor.GREEN + "Enter End");
                score.setScore(4);
                break;
            case 1:
                score = objective.getScore(ChatColor.GREEN + "Kill Enderdragon");
                score.setScore(2);
                break;
        }
        String empty = "--:--:--";
        if (time.equals("")) {
            score = objective.getScore(ChatColor.YELLOW + empty);
        } else {
            score = objective.getScore(ChatColor.YELLOW + time);
        }
        score.setScore(i);
    }

    private void clearTime(String time) {
        if(!time.equals(""))
            board.resetScores(ChatColor.YELLOW + time);
    }

    //TODO del score from old user
    public void timeScoreBoard() {
        BukkitRunnable run = new BukkitRunnable() {
            @Override
            public void run() {
                int lastIndex = playerIndex -1;
                if (playerIndex > players.length -1) {
                    playerIndex = 0;
                    lastIndex = players.length -1;
                }
                objective.setDisplaySlot(DisplaySlot.SIDEBAR);
                objective.setDisplayName("Times");

                String empty = "--:--:--";
                Player p = players[playerIndex];
                Speedrun speed = runs.get(p.getName());
                playerIndex++;

                String oldPlayer = players[lastIndex].getName();
                Speedrun oldSpeed = runs.get(oldPlayer);
                if(!firstLoop)
                    board.resetScores(ChatColor.RED + oldPlayer);

                //Name Title
                Score score = objective.getScore(ChatColor.RED + p.getName());
                score.setScore(12);

                score = objective.getScore("");
                score.setScore(11);
                board.resetScores(ChatColor.YELLOW + empty);

                String[] splitNames = {"nether", "fortress", "exit_nether", "end", "kill_enderdragon"};
                for(int i = 0, j = 9; i < splitNames.length; i++, j =- 2) {
                    if (!firstLoop) {
                        String oldTime = oldSpeed.getTimeSplit(splitNames[i]);
                        clearTime(oldTime);
                    }
                    String time = speed.getTimeSplit(splitNames[i]);
                    setScoreBoard(j, time);
                    if (time.equals(""))
                        break;
                    if (i == 4)
                        speed.exitNether = time;
                }

                firstLoop = false;
                for(Player player: players)
                    player.setScoreboard(board);

            }
        };
        Plugin x = Bukkit.getPluginManager().getPlugin("SpeedrunPlugin");
        run.runTaskTimerAsynchronously(x, 0, 80);
    }

    public void restart() {
        for(Player p: players) {
            Speedrun run = runs.get(p.getName());
            run.stop();
            MultiverseCore core = (com.onarandombox.MultiverseCore.MultiverseCore) Bukkit.getPluginManager().getPlugin("Multiverse-Core");
            DestinationFactory df = core.getDestFactory();
            MVDestination d = df.getDestination("world");
            MVTeleportEvent teleportEvent = new MVTeleportEvent(d, p, p, true);
            core.getServer().getPluginManager().callEvent(teleportEvent);
            Teleporter teleportObject = (d instanceof CustomTeleporterDestination) ?
                    ((CustomTeleporterDestination) d).getTeleporter() : core.getSafeTTeleporter();
            TeleportResult result = teleportObject.teleport(p, p, d);
        }
        createOwnerWorld();
        for(Player p: players)
            if(p != owner)
                createPlayerWorld(p);
        runs = new HashMap<>();
        start();
    }

    public void start() {
        if(ownerWorld && playerWorld) {
            ScoreboardManager manager = Bukkit.getScoreboardManager();
            board = manager.getNewScoreboard();
            objective = board.registerNewObjective("test", "dummy");
            timeScoreBoard();
            MultiverseCore core = (com.onarandombox.MultiverseCore.MultiverseCore) Bukkit.getPluginManager().getPlugin("Multiverse-Core");
            DestinationFactory df = core.getDestFactory();
            Timestamp time = new Timestamp(System.currentTimeMillis() + 10000);
            for(Player p: players) {
                MVDestination d = df.getDestination(p.getName());
                MVTeleportEvent teleportEvent = new MVTeleportEvent(d, p, p, true);
                core.getServer().getPluginManager().callEvent(teleportEvent);
                Speedrun speed = new Speedrun(p, this, time);
                speed.start();
                runs.put(p.getName(), speed);
                Teleporter teleportObject = (d instanceof CustomTeleporterDestination) ?
                        ((CustomTeleporterDestination)d).getTeleporter() : core.getSafeTTeleporter();
                TeleportResult result = teleportObject.teleport(p, p, d);
            }
        } else {
            owner.sendMessage(ChatColor.RED + "Error starting, not all worlds are created");
        }
    }
}

