package farm.cocaine.speedrunplugin;

import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.advancement.Advancement;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class SpeedrunPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        CommandParty party = new CommandParty();
        this.getCommand("party").setExecutor(party);
        //this.getServer().getPluginManager().registerEvents(party, this);
        // Plugin startup logic
        //Bukkit.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

}
